package com.enthu.urbox;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothProfile;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.enthu.urbox.activity.BrowserActivity;
import com.enthu.urbox.bluetoothdatamodel.parsing.Device;
import com.enthu.urbox.bluetoothdatamodel.parsing.Engine;
import com.enthu.urbox.services.Write;

import java.util.Arrays;

import butterknife.InjectView;

import static com.enthu.urbox.services.BluetoothLeService.ACTION_GATT_CONNECTED;
import static com.enthu.urbox.services.BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR;
import static com.enthu.urbox.services.BluetoothLeService.ACTION_GATT_DISCONNECTED;


public class DatapushActivity extends AppCompatActivity {
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    EditText text, batLowtext, textHigh, textSmsDry, textSmsWet, textSmsMedium,txtconf,oxy;
    Button btn, battLow, bttnHigh, bttnsmsWet, bttsmsDry, bttsmsMedium, btSSid,btpass,btnoxy;
    BluetoothGatt magattService;
    @InjectView(R.id.help_button)
    TextView helpButton;
    Button Latbtn, longbtn;
    TextView txtLat, txtlan,lattxt,longtxt;
    String lat;
    String provider;
    protected String latitude, longitude;
    protected boolean gps_enabled, network_enabled;
    private Dialog helpDialog;
    private Dialog hiddenDebugDialog;
    private String DEVICE_ADDRESS;
    private Dialog alertDialogView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datapush);

//        findViewById(R.id.buttonShowDialog).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                //calling this method to show our android custom alert dialog
//                showCustomDialog();
//            }
//        });


//Editext

        textSmsWet = findViewById(R.id.smsWet);
//        batLowtext = findViewById(R.id.batteryLow);
//        textSmsDry = findViewById(R.id.smsDry);
        textSmsMedium = findViewById(R.id.smsMedium);
//        txtconf=findViewById(R.id.timetxt);
        oxy=findViewById(R.id.oxygen);
//        lattxt = findViewById(R.id.Latitude);
//        longtxt = findViewById(R.id.Longitude);


        //Button


//        battLow = findViewById(R.id.batLowThersholdUpdate);
//        bttsmsDry = findViewById(R.id.dryThersholdUpdate);
        btSSid = findViewById(R.id.SSID);
        btpass = findViewById(R.id.Passwdbtn);
//        btnconf=findViewById(R.id.bttnconf);
        btnoxy=findViewById(R.id.Oxythrsbtn);
//        bttnlat = findViewById(R.id.LatUpdate);
//        bttnlong = findViewById(R.id.LongUpdate);


//        btnhelp=findViewById(R.id.help_button);
        //gatt  service
        magattService = Write.gattService;

        if (magattService == null) {

            Toast.makeText(this, "Device Has benn disconnected ", Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, BrowserActivity.class);
            startActivity(i);
        }


        //validation For all the Fileds


        //=============== Oxygen Threshold ========================//


        btSSid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

//                if ( oxy.length()==5  && textSmsWet.length() >3 && textSmsMedium.length() >3 ){


                    String value1 = textSmsWet.getText().toString();
                    if (value1.length() <= 30 && !(value1.length() == 0)) {
                        byte[] newValue1 = value1.getBytes();
                        System.out.println(newValue1);

                        System.out.println(Arrays.toString(newValue1));
                        Write.SSID.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                        Write.SSID.setValue(newValue1);
                        textSmsWet.setText("");
                        //    writeCharacteristic(Write.batLowThershold);
                        if (value1 != null) {

                            byte[] newValue01 = value1.getBytes();

                            try {
                                Write.SSID.setValue(newValue01);
                                magattService.writeCharacteristic(Write.SSID);

                            } catch (Exception e) {
                                Log.e("Service", "null" + e);
                            }
                        } else {
                            Write.SSID.setValue(value1);
                            magattService.writeCharacteristic(Write.SSID);
                        }
                        Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                    } else {
                        textSmsWet.setError("Enter valid SSID");
                    }
            }
        });

      //========================== Wifi Password  ========================
            btpass.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

              String value2 = textSmsMedium.getText().toString();
              if (value2.length() <= 30 && !(value2.length() == 0)) {

                  byte[] newValue2 = value2.getBytes();
                  System.out.println(newValue2);

                  System.out.println(Arrays.toString(newValue2));
                  Write.Password.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                  Write.Password.setValue(newValue2);
                  textSmsMedium.setText("");
                  //    writeCharacteristic(Write.batLowThershold);
                  if (value2 != null) {

                      byte[] newValue11 = value2.getBytes();

                      try {
                          Write.Password.setValue(newValue11);
                          magattService.writeCharacteristic(Write.Password);

                      } catch (Exception e) {
                          Log.e("Service", "null" + e);
                      }
                  } else {
                      Write.Password.setValue(value2);
                      magattService.writeCharacteristic(Write.Password);
                  }
                  Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                  Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
              } else {
                  textSmsMedium.setError("Enter valid Password");
              }

                }
            });

            btnoxy.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    String value = oxy.getText().toString();
                    if (value.length() == 5 && !(value.length()==0)) {
                        if(value.charAt(2)=='.'){

                            byte[] newValue = value.getBytes();
                            System.out.println(newValue);

                            System.out.println(Arrays.toString(newValue));
                            Write.Oxygen.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                            Write.Oxygen.setValue(newValue);
                            oxy.setText("");
                            //    writeCharacteristic(Write.batLowThershold);
                            if (value != null) {

                                byte[] newValue1 = value.getBytes();

                                try {
                                    Write.Oxygen.setValue(newValue1);
                                    magattService.writeCharacteristic(Write.Oxygen);

                                } catch (Exception e) {
                                    Log.e("Service", "null" + e);
                                }
                            } else {
                                Write.Oxygen.setValue(value);
                                magattService.writeCharacteristic(Write.Oxygen);
                            }
                            Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                            Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();

                        }else {
                            oxy.setError("Enter ' . ' is 3rd digit");
                        }
                    }else {
                        oxy.setError("Enter valid Threshold");
                    }



//                }
//                else {
//                    oxy.setError("Please Enter Valid");
//                    textSmsWet.setError("Please Enter Valid");
//                    textSmsMedium.setError("Please Enter Valid");
//                }
          }
      });


        //=============================             ======================//


//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        if (ActivityCompat.checkSelfPermission(DatapushActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DatapushActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, DatapushActivity.this);
//
    }
//
//    @Override
//    public void onLocationChanged(Location location) {
//        txtLat = (TextView) findViewById(R.id.Latitude);
//        txtlan = (TextView) findViewById(R.id.Longitude);
//
//        txtLat.setText("" + location.getLatitude());
//        txtlan.setText("" + location.getLongitude());
//
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//        Log.d("Latitude","disable");
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//        Log.d("Latitude","enable");
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//        Log.d("Latitude","status");
//    }

    public boolean writeCharacteristic(BluetoothGattCharacteristic charac){

        //check mBluetoothGatt is available
        if (magattService == null) {
            Log.e("", "lost connection");
            return false;
        }
//        if (Service == null) {
//            Log.e("", "service not found!");
//            return false;
//        }
        if (charac == null) {
            Log.e("", "char not found!");
            return false;
        }

        boolean status = magattService.writeCharacteristic(charac);
        return status;
    }

    public byte[] hexToByteArray(String hex) {

        if (hex.length() != 0 && hex.length() % 2 != 0) {
            hex = "0" + hex;
        }

        int len =hex.length();

        byte[] byteArr = new byte[len];
        for (int i = 0; i < byteArr.length; i++) {
            int init = i * 2;
            int end = init + 2;
            int temp = Integer.parseInt(hex.substring(init, end), 16);
            byteArr[i] = (byte) (temp & 0xFF);
        }
        return byteArr;
    }
    // Converts string given in decimal system to byte array
    private byte[] decToByteArray(String dec) {
        if (dec.length() == 0) {
            return new byte[]{};
        }

        String data[] = new String[4];
        for (int i = 0;i < dec.length(); i++){
            data[i]= String.valueOf(dec.charAt(i));
        }
        byte[] byteArr = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            try {
                byteArr[i] = (byte) (Integer.parseInt(data[i]));
            } catch (NumberFormatException e) {
                return new byte[]{0};
            }
        }
        return byteArr;
    }
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.activity_help, viewGroup, false);



        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        private Device connectedDevice;

        // Called when device has changed connection status and appropriate
        // broadcast with device address extra is sent
        // It can be either connected or disconnected state
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    Device device = Engine.getInstance().getDevice(gatt);
                    device.setConnected(true);
                    connectedDevice = device;
                    Intent updateIntent = new Intent(ACTION_GATT_CONNECTED);
                    updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                    sendBroadcast(updateIntent);
                    gatt.discoverServices();
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    Device device = Engine.getInstance().getDevice(gatt);
                    device.setConnected(false);
                    Intent updateIntent = new Intent(ACTION_GATT_DISCONNECTED);
                    updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                    if (device.equals(connectedDevice)) {
                        connectedDevice = null;
                    }
                    sendBroadcast(updateIntent);
                }
            } else {
                Device device = Engine.getInstance().getDevice(gatt);
                Intent updateIntent = new Intent(ACTION_GATT_CONNECTION_STATE_ERROR);
                updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                sendBroadcast(updateIntent);
            }
            Log.i("BLE service", "onConnectionStateChange - status: " + status + " - new state: " + newState);

        }};




    }