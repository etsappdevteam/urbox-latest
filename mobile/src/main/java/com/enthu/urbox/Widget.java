package com.enthu.urbox;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

public class Widget extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget);
        WebView view = new WebView(this);
        view.getSettings().setDomStorageEnabled(true);
        view.getSettings().setJavaScriptEnabled(true);


        view.loadUrl("file:///android_asset/widget.html");
        setContentView(view);
        view.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url){
                System.out.println("hello World");
                Integer data=20;
                view.loadUrl("javascript:myFunction(50);");
            }
        });
    }

}