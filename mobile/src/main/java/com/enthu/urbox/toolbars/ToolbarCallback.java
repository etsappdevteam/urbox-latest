package com.enthu.urbox.toolbars;

import com.enthu.urbox.utils.FilterDeviceParams;

public interface ToolbarCallback {
    void close();

    void submit(FilterDeviceParams filterDeviceParams, boolean close);

}
