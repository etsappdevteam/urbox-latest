package com.enthu.urbox.utils;

public enum ToolbarName {
    CONNECTIONS, LOGS, FILTER
}
